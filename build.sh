#!/bin/sh
# Requires pandoc, texlive and eisvogel latex template https://github.com/Wandmalfarbe/pandoc-latex-template
pandoc report.md metadata.yaml -o health-check-user-stories.pdf --from markdown --template eisvogel --listings
