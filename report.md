
May, 2021

Published by Magma Collective
info@magmacollective.org

# CoBox Health Check User Stories

## Template

- As a <user type>, when I <visit somewhere>, I want to <perform some task> so that I can <achieve some goal>

## From the perspective of a space member (on the client app)

### General Improvements

- As a space member, when I nagivate to my space #show page, I want to see my address with a copy button.
- As a space member, when I navigate to my space #show page, I want to see some kind of indicator saying whether I'm online or not (i.e. able to replicate).
- As a space member, when I navigate to my space #show page, I want to see a slider/button enabling me to switch my online availability on or off, so that I can save my bandwidth or CPU.

### Space Member Presence

- As a space member, when I navigate to my space #show page, I want to see a little green dot next to a space member's name/pubkey when they are online, so I know that they are online.
- As a space member, when I navigate to my space #show page, I want to see a little red dot next to a space member's name/pubkey when they are offline, so I know that they are offline.
- As a space member, when I navigate to my space #show page, I want to see a timestamp next to a space members name/pubkey displaying the 'last seen by me' time, so I know the last time the space member was in the space and replicating with me.
- As a space member, when I navigate to my space #show page, I want to see a little sync icon in place of the traffic light dot next to space member's name/pubkey so that I know we are replicating right now.

### Health Tab a.k.a. Seeder Presence 

- As a space member, when I navigate to my space #show page, and I click on the health tab, I want to see a list of the remote peers' names / public keys whom I know to be seeding the space, so that I can identify these peers.
- For each peer I want to know when was the last time we replicated with each other / synchronised our feeds.
- As a space member, when I navigate to my space #show page, and I click on the health tab, I want to see a number at the top that tells me how many peers/seeders are sharing/backing up this space.
- As a space member, when I navigate to my space #show page, and I click on the health tab, I want to see the number of peers/seeders with whom I have fully replicated within the tolerance window, which is a time-period that I can set.
- As a space member, I want to know how many of my peers in a given space have failed the proof of storage, so that I can get a rapid overview of the health of that space.

#### General Metrics for Health

- How many peers do we have per space.
- When was each peer's last sync with your version of the space.
- Is the always-online 'seeder' of the space currently online? if no - when was the last time it was?
- When was the last time you were online / connected with anyone - ie when was the last time you synced your version of the space.

#### Peer-specific Health

- As a space member, when I navigate to my space, and I click on the health tab, I want to see a little green dot next to each remote peer's name/pubkey so I know we performed a full replication / sync recently (and therefore *should be* backing up my space right now).
- As a space member, when I navigate to my space, and I click on the health tab, I want to see a little amber dot next to each remote peer's name/pubkey when we fail to complete a full replication / sync (and therefore some attention should be paid to this).
- As a space member, when I navigate to my space, and I click on the health tab, I want to see a little red dot next to each remote peer's name/pubkey so I know we have not completed a full replication / sync within our tolerance window.

#### Aggregated Health

- As a space member, when I navigate to my space, and I click on the health tab, at the top of the page, I want to see a highly visible red warning telling me that n of my backups are not up-to-date within my tolerance margin, so that I should take action and investigate why these backups are not up to date.
- As a space member, when I navigate to my space, and I click on the health tab, at the top of the page, I want to see a highly visible green sign telling me that all my backups are up to date, so I know that I don't need to do anything.
- As a space member, when I navigate to my space, and I click on the health tab, at the top of the page, I want to see a highly visible amber sign telling me that some backups are not entirely up to date, some might be outside/approaching of my tolerance zone, some might have failed to sync, so I know that I might need to take action in future.
- As a space member, when I navigate to my space #show page, and I click on the health tab, I want to be able to click on a arrow above the 'last backed up' field so that it re-sorts the seeders in descending order of 'last backed up' (from most recent), so that I can quickly see who the most recently seen seeders are.
- As a space member, when I navigate to my space #show page, and I click on the health tab, I want to be able to click on a arrow above the 'last backed up' field so that it re-sorts the seeders in ascending order of 'last_backed_up' (from least recent), so that I can quickly see who the least recently seen seeders are.
- As a space member, when I navigate to my space #show page, and I click on the health tab, I want to be able to remove seeders who haven't been seen for a really long time, so that I can tidy up my seeders list.

### Space Settings

- As a space member, when I navigate to my space, I want to see a 'settings' tab/icon so that I can change space-specific settings.
- As a space member, when I nagivate to my space settings tab, I want to set a threshold specifying how many seeders must be online to ensure our backups are in the 'green', so that I can configure by backup alerts.
- As a space member, when I navigate to my space settings tab, I want to set a tolerance margin for last seeing seeders, so that I can adjust my own comfort settings for how regularly my backups need to be made.

### Proof of Storage

- As a space member, when I navigate to my space #show health tab, I want to be able to click on a seeder who is online and request a proof of backup.
- As a space member, when I navigate to my space #show health tab, I want to see a warning next to a seeder who has failed the proof of storage. When I click on that warning, it directs me to a set of recommendations, e.g. getting in touch with that seeder directly and asking why its failed, setting up a new seeder to account for the broken one.

### From the perspective of a seeder/server admin, using the client app

- As a seeder admin, when I navigate to my seeder #index page, next to the name of each of the seeders that I am an admin for I want to see a 'last full sync (with anyone on that space)' timestamp, so that I can assess whether my seeder is still replicating.
- As a seeder admin, when I navigate to my seeder #show page, I want to see a breakdown of the seeder's health, including its uptime, so that I can assess the status of my seeder.
- As a seeder admin, I want my seeder to send me a 'status' object so that I know what its currently doing. This object should include the following:
    - seeder uptime 
    - list of the spaces being replicated right now
    - seeder's current disk-space
- As a seeder, when I visit a space's address to perform a backup, I want to record when I meet peers, who they are, and whether I performed a full sync. Using this, I can know about when I last connected with a remote peer, so I know if I am effectively seeding that space.

## From the perspective of a seeder, using the seeder CLI on a remote seeder 'box'

- The seeder app is a CLI interface. Logging into the seeder CLI on my remote seeder 'box', I want to see the same information as that which I see for each space as a space member, i.e. a list of peers for each seed, last sync timestamp and with whom.
- As a seeder, when I execute the command `cobox-seeder seeds ls`, i want to see a list of seeds, nested within each I want to see a list of peer pubkeys, and their last sync time, which enables me to assess who and how many are most up to date.
- As a seeder, when I execute the command `cobox-seeder seeds status <name/address>`, I want to see a breakdown of this same information, with some more compact summary metrics, such as how many remote peers we've seen within the last day, 2 days, week, month, ... , so that I can quickly assess the state of this backup.
- As a seeder, when I execute the command `cobox-seeder status`, I want to see a comprehensive breakdown of my entire seeder state, including application up time, number of backups, their peers, last sync, space occupied by each backup, current overall hard drive space, remaining hard drive space, etc, so I can quickly assess the state of my seeder.
- As a seeder, I want to be able to configure publishing seeder status information in the admin log so that each admin can read some of this information from the client UI.

# Appendix: Support

The creation of CoBox Health Check User Stories, as well as implementation of the outcomes, were supported by the Bundesministerium für Bildung und Forschung and the Prototype Fund.
  
![Bundesministerium für Bildung und Forschung - BMBF](./assets/BMBF-logo.png) ![Prototype Fund](./assets/PrototypeFund-logo.png)
